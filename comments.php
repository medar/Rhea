
<?php if(comments_open()) : ?>

<div class="comment-sec">
	<h3 class="heading3-border text-uppercase"><?php comments_number(  ); ?> к <?php the_title('”', '”'); ?></h3>
	<?php
	$args = array(
		'type'      => 'comment',
		'max_depth' => 2,
		'style'     => 'div',
		'callback'  => 'rhea_custom_comments',
	);
	wp_list_comments($args); ?>
</div>

<?php

$args = array(
	'fields'               => array(
		'author' => '<div class="form-group"><div class="row"><div class="col-xs-12 col-sm-6 col-md-6"><input type="text" class="form-control" placeholder="Name"></div>',
		'email'  => '<div class="col-xs-12 col-sm-6 col-md-6"><input type="email" class="col-xs-12 col-md-6 form-control" placeholder="Email"></div></div></div>',
		'url'    => ''
	),
	'comment_notes_before' => '',
	'comment_notes_after'  => '',
	'title_reply'          => '',
	'title_reply_before'   => '',
	'title_reply_after'    => '',
	'class_form'           => '',
	'comment_field'        => '<div class="form-group"><textarea class="form-control" placeholder="Message"></textarea></div>',
	'submit_button'        => '		<div class="form-group"><button type="submit" class="btn btn-submit">Submit</button></div>
'
);

?>

<div class="comment-form">
	<h3>Оставьте комментарий</h3>
	<p>Ваш e-mail не будет опубликован</p>
	<?php comment_form( $args ); ?>
</div>

<?php endif; ?>