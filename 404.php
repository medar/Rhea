<?php get_header(); ?>

<!-- Breadcroumbs start -->
<div class="wshipping-content-block wshipping-breadcroumb inner-bg-1">
	<div class="container">
		<div class="row">
			<div class="col-md-7">
				<h2>Ошибка 404</h2>
				<?php
				if ( function_exists( 'yoast_breadcrumb' ) ) {
					yoast_breadcrumb( '<p id="breadcrumbs">', '</p>' );
				}
				?>
			</div>
		</div>
	</div>
</div>
<!-- Breadcroumbs end -->

<!-- Error 404 Start -->
<div class="wshipping-content-block">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1">
				<div class="error-section">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/404.png" alt="404"/>
					<h1>Страница не найдена</h1>
					<?php the_field('404', 'option'); ?>
					<a href="/" class="wshipping-button cta-btn">На главную</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Error 404 end -->

<?php get_footer(); ?>
