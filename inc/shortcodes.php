<?php

function rhea_shortcode_blockquote_style2( $atts, $content ) {
	$atts = array_change_key_case( (array) $atts, CASE_LOWER );

	$output = '';
	ob_start();
	?>
	<div class="blockquote-style2">
		<p><?php echo $content; ?></p>
	</div>
	<?php
	$output .= ob_get_clean();

	return $output;
}

function rhea_shortcodes_init() {
	add_shortcode( 'blockquote_style2', 'rhea_shortcode_blockquote_style2' );
}

add_action( 'init', 'rhea_shortcodes_init' );