<?php
function register_custom_types() {
	register_post_type( 'company_news', array(
		'label'               => null,
		'labels'              => array(
			'name'               => 'News',
			'singular_name'      => 'News',
			'add_new'            => 'Add news',
			'add_new_item'       => 'Add news',
			'edit_item'          => 'Edit news',
			'new_item'           => 'New news',
			'view_item'          => 'View news',
			'search_items'       => 'Search news',
			'not_found'          => 'Not found',
			'not_found_in_trash' => 'Not found in trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'Company News',
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => false,
		'show_in_rest'        => false,
		'rest_base'           => false,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-id-alt',
		//'capability_type'   => 'post',
		//'capabilities'      => 'post',
		//'map_meta_cap'      => null,
		'hierarchical'        => false,
		'supports'            => array( 'title', 'editor' ),
		// 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => array(),
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	) );
	register_post_type( 'our_products', array(
		'label'               => null,
		'labels'              => array(
			'name'               => 'Products',
			'singular_name'      => 'Product',
			'add_new'            => 'Add product',
			'add_new_item'       => 'Add product',
			'edit_item'          => 'Edit product',
			'new_item'           => 'New product',
			'view_item'          => 'View product',
			'search_items'       => 'Search product',
			'not_found'          => 'Not found',
			'not_found_in_trash' => 'Not found in trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'Our Products',
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => false,
		'show_in_rest'        => false,
		'rest_base'           => false,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-products',
		//'capability_type'   => 'post',
		//'capabilities'      => 'post',
		//'map_meta_cap'      => null,
		'hierarchical'        => false,
		'supports'            => array( 'title', 'editor', 'thumbnail' ),
		// 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => array(),
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	) );
	register_post_type( 'our_services', array(
		'label'               => null,
		'labels'              => array(
			'name'               => 'Services',
			'singular_name'      => 'Service',
			'add_new'            => 'Add service',
			'add_new_item'       => 'Add service',
			'edit_item'          => 'Edit service',
			'new_item'           => 'New service',
			'view_item'          => 'View service',
			'search_items'       => 'Search service',
			'not_found'          => 'Not found',
			'not_found_in_trash' => 'Not found in trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'Our Services',
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => false,
		'show_in_rest'        => false,
		'rest_base'           => false,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-admin-settings',
		//'capability_type'   => 'post',
		//'capabilities'      => 'post',
		//'map_meta_cap'      => null,
		'hierarchical'        => false,
		'supports'            => array( 'title', 'editor', 'thumbnail' ),
		// 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => array('services'),
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	) );
	register_post_type( 'how_we_work', array(
		'label'               => null,
		'labels'              => array(
			'name'               => 'How we work',
			'singular_name'      => 'How we work',
			'add_new'            => 'Add How we work',
			'add_new_item'       => 'Add How we work item',
			'edit_item'          => 'Edit How we work item',
			'new_item'           => 'New How we work item',
			'view_item'          => 'View How we work item',
			'search_items'       => 'Search How we work items',
			'not_found'          => 'Not found',
			'not_found_in_trash' => 'Not found in trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'How we work',
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => false,
		'show_in_rest'        => false,
		'rest_base'           => false,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-businessman',
		//'capability_type'   => 'post',
		//'capabilities'      => 'post',
		//'map_meta_cap'      => null,
		'hierarchical'        => false,
		'supports'            => array( 'title', 'editor', 'thumbnail' ),
		// 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => array(),
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	) );

}

function create_taxonomy(){
		register_taxonomy('services', array('our_services'), array(
		'label'                 => '',
		'labels'                => array(
			'name'              => 'Services category',
			'singular_name'     => 'Services category',
			'search_items'      => 'Search Services',
			'all_items'         => 'All Services',
			'view_item '        => 'View Services',
			'parent_item'       => 'Parent Services',
			'parent_item_colon' => 'Parent Services:',
			'edit_item'         => 'Edit Services',
			'update_item'       => 'Update Services',
			'add_new_item'      => 'Add New Services',
			'new_item_name'     => 'New Services Name',
			'menu_name'         => 'Services category',
		),
		'description'           => '', // описание таксономии
		'public'                => true,
		'publicly_queryable'    => null, // равен аргументу public
		'show_in_nav_menus'     => true, // равен аргументу public
		'show_ui'               => true, // равен аргументу public
		'show_tagcloud'         => true, // равен аргументу show_ui
		'show_in_rest'          => null, // добавить в REST API
		'rest_base'             => null, // $taxonomy
		'hierarchical'          => true,
		'update_count_callback' => '',
		'rewrite'               => true,
		//'query_var'             => $taxonomy, // название параметра запроса
		'capabilities'          => array(),
		'meta_box_cb'           => null, // callback функция. Отвечает за html код метабокса (с версии 3.8): post_categories_meta_box или post_tags_meta_box. Если указать false, то метабокс будет отключен вообще
		'show_admin_column'     => false, // Позволить или нет авто-создание колонки таксономии в таблице ассоциированного типа записи. (с версии 3.5)
		'_builtin'              => false,
		'show_in_quick_edit'    => null, // по умолчанию значение show_ui
	) );
}

add_action('init', 'create_taxonomy');
add_action( 'init', 'register_custom_types' );