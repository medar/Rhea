<?php
/**
 * Template Name: Contacts
 *
 * Description: Template for Contacts page
 */
get_header();
the_post();
?>

<!-- Breadcroumbs start -->
<div class="wshipping-content-block wshipping-breadcroumb inner-bg-1">
	<div class="container">
		<div class="row">
			<div class="col-md-7">
				<h2>Свяжитесь с нами</h2>
				<a href="/" title="Home">Главная</a> / Контакты
			</div>
			<div class="col-md-5 text-right"><h4>Доставка и монтаж
					во Всех городах Украины, <span>БЫСТРО</span> и <span>КАЧЕСТВЕННО!</span></h4></div>
		</div>
	</div>
</div>
<!-- Breadcroumbs end -->

<!-- Contact Section Start -->
<div class="wshipping-content-block">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-4 pull-right">
				<?php foreach ( get_field( 'address' ) as $address_block ) : ?>
					<div class="address">
						<h3><?php echo $address_block['header']; ?></h3>
						<div class="address-block">
							<ul>
								<?php foreach ( $address_block['address_item'] as $address_item ) : ?>
									<li>
										<?php echo $address_item['icon']; ?>
										<strong><?php echo $address_item['title']; ?>:</strong>
										<br>
										<?php echo $address_item['content']; ?>
									</li>
								<?php endforeach; ?>
							</ul>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-8">
				<div class="contact-text">
					<?php the_content(); ?>
				</div>
				<div class="contact-form">
					<h3 class="heading3-border text-uppercase">Быстрый запрос</h3>
					<?php echo do_shortcode('[contact-form-7 id="300" title="Форма на странице КОНТАКТЫ"]'); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Contact Section end -->

<!-- Map start -->
<div class="map">
	<?php the_field('map'); ?>
</div>
<!-- Map end -->

<?php get_footer(); ?>
