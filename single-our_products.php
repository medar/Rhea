<?php get_header();

while ( have_posts() ) :
	the_post();
	?>

	<!-- Slider Start -->
	<?php get_template_part( 'template-parts/slider', 'main' ); ?>
	<!-- Slider End -->

	<?php $banners = get_field( 'banners' );
	if ( $banners ) :
		?>
		<div class="wshipping-content-block wshipping-breadcroumb inner-bg-1 banner__wrapper">
			<div class="container">
				<div class="row">
					<?php foreach ( $banners as $banner ) : ?>
						<div class="banner col-xs-6 col-sm-3">
							<a href="<?php echo esc_attr( $banner['href'] ); ?>"><?php echo $banner['title']; ?>
								<?php echo wp_get_attachment_image( $banner['img'], 'medium' ); ?>
							</a>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>

	<!-- About content start -->
	<div class="wshipping-content-block">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-9 pull-right">
					<div class="right-block">
						<?php
						$mini_slider = get_field( 'mini-slider' );
						if ( ! empty( $mini_slider ) ) :
							?>
							<!-- Slider Content Start -->
							<div class="homepage-slides-wrapper">
								<div class="homepage-slides slider--disable--autoplay">
									<?php foreach ( $mini_slider as $slide ) : ?>
										<div class="single-slide-item2"
											 style="background-image: url(<?php echo $slide['img']; ?>)">
											<div class="item-table">
												<div class="item-tablecell">
													<div class="container">
														<div class="row">
															<div class="col-md-10 col-md-offset-1">
																<h1><?php echo $slide['header']; ?></h1>
																<?php echo wpautop( $slide['content'] ); ?>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
							<!-- Slider Content End -->
						<?php endif; ?>
						<?php
						if ( function_exists( 'yoast_breadcrumb' ) ) {
							yoast_breadcrumb( '<p id="breadcrumbs">', '</p>' );
						}
						?>
						<!-- Paragraph Start -->
						<h2 class="heading2-border"><?php the_title(); ?></h2>
						<?php the_content(); ?>
						<!-- Paragraph End -->

						<?php
						$video_gallery = get_field( 'video_gallery' );
						$videos        = $video_gallery['videos'];
						if ( $videos ) :
							?>
							<div class="video-gallery row">
								<div class="col-xs-12">
									<h2 class="heading2-border"><?php echo $video_gallery['header']; ?></h2>
								</div>
								<?php
								foreach ( $videos as $video ) :
									$video = $video['video'];
									$video_id = rhea_get_youtube_video_id( $video );
									$url = rhea_get_youtube_video_thumbnail_url( $video );
									?>
									<div class="col-sm-4 mb15">
										<a class="popup-youtube"
										   href="https://www.youtube.com/watch?v=<?php echo $video_id; ?>">
											<img src="<?php echo $url; ?>">
										</a>
									</div>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
						<!-- Нас выбирают -->
						<?php $choose = get_field( 'why_choose_us' );
						if ( ! empty( $choose['header'] ) ) :
							?>
							<div class="why-choose-us-inner wow fadeInUp">
								<h3 class="heading3-border text-uppercase"><?php echo $choose['header']; ?></h3>
								<div class="row">
									<div class="col-xs-12 col-sm-6 col-md-3">
										<div class="why-choose-us-icon">
											<i class="fa fa-handshake-o"></i>
											<h5><?php echo $choose['text_1']; ?></h5>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-3">
										<div class="why-choose-us-icon">
											<i class="fa fa-diamond"></i>
											<h5><?php echo $choose['text_2']; ?></h5>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-3">
										<div class="why-choose-us-icon">
											<i class="fa fa-thumbs-o-up"></i>
											<h5><?php echo $choose['text_3']; ?></h5>
										</div>
									</div>
									<div class="col-xs-12 col-sm-6 col-md-3">
										<div class="why-choose-us-icon">
											<i class="fa fa-map-marker"></i>
											<h5><?php echo $choose['text_4']; ?></h5>
										</div>
									</div>
								</div>
							</div>
						<?php endif; ?>
						<!-- Нас выбирают конец -->
						<?php
						if ( ! empty( get_field( 'content_2' ) ) ) :
							the_field( 'content_2' );
						endif;
						?>

						<!-- Textbox Start -->
						<?php $advantages = get_field( 'advantages' );
						if ( ! empty( $advantages['text_1'] ) ) :
							?>
							<div class="col-md-12 small-pt40">
								<h2 class="heading2-border"><?php echo $advantages['header']; ?></h2>
							</div>
							<div class="col-sm-4">
								<div class="text-box text-box-style1"><?php echo $advantages['text_1']; ?></div>
							</div>
							<div class="col-sm-4">
								<div class="text-box text-box-style1"><?php echo $advantages['text_2']; ?></div>
							</div>
							<div class="col-sm-4">
								<div class="text-box text-box-style1"><?php echo $advantages['text_3']; ?></div>
							</div>
						<?php endif; ?>

						<!-- Textbox End -->

						<!-- Gallery Start -->
						<?php $gallery = get_field( 'gallery' ); ?>
						<?php if ( ! empty( $gallery['img'] ) ) : ?>
							<div class="col-md-12 pt40">
								<h2 class="heading2-border"><?php echo $gallery['header']; ?></h2>
							</div>
							<div class="wshipping-content-block col-md-12">
								<div class="row" id="freightGallery">
									<?php foreach ( $gallery['img'] as $img ) : ?>
										<div class="fGallery air col-sm-4" data-cat="air">
											<div class="fGallery-wrapper">
												<a class="latest-gallery"
												   href="<?php echo $img['url']; ?>"
												   title="Choose Your Freight" data-effect="fadeIn">
													<i class="fa fa-search-plus" aria-hidden="true"></i>
													<?php echo wp_get_attachment_image( $img['ID'], 'large' ); ?>
												</a>
												<div class="freight-caption">
													<div class="label-text">
														<span class="text-title"><?php echo $img['title'] ?></span>
													</div>
													<div class="label-bg"></div>
												</div>
											</div>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
						<?php endif; ?>
						<!-- Gallery end -->

						<?php get_template_part( 'template-parts/block', 'related' ); ?>

						<!-- List Style Start -->
						<?php the_field( 'content_3' );
						$tag_links = get_field( 'tag_links' );
						if ( ! empty( $tag_links ) ) : ?>
							<div class="tags">
								<h3 class="heading3-border text-uppercase"><?php the_field( 'tags_header', $page_id ); ?></h3>
								<?php foreach ( $tag_links as $tag_link ) : ?>
									<a href="<?php echo $tag_link['link_href']; ?>"
									   title="<?php echo $tag_link['link_text']; ?>"><?php echo $tag_link['link_text']; ?></a>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-3">
					<?php rhea_get_sidebar( 'products' ); ?>
				</div>
			</div>
		</div>
	</div>
	<!-- About content end -->

	<!-- Counter start -->
	<?php //dumper(get_field('counter'));
	$counter = get_field( 'counter' ); ?>
	<div class="wshipping-content-block counter-section" style="background-image: url('<?php echo $counter['bg']; ?>')">
		<div class="container wow fadeInUp">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="counter-block">
						<span class="counter-icon"><i class="fa fa-linode "></i></span>
						<p><?php echo $counter['text_1']; ?></p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="counter-block">
						<span class="counter-icon"><i class="fa fa-usd"></i></span>
						<p><?php echo $counter['text_2']; ?></p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="counter-block">
						<span class="counter-icon"><i class="fa fa-thumbs-up"></i></span>
						<p><?php echo $counter['text_3']; ?></p>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="counter-block">
						<span class="counter-icon"><i class="fa fa-truck"></i></span>
						<p><?php echo $counter['text_4']; ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Counter End -->

	<?php
	$services = get_field( 'services' );
	if ( $services ) :
		foreach ( $services as $service_block ) : ?>
			<div class="wshipping-content-block meet-the-team">
				<div class="container wow fadeInUp">
					<div class="row">
						<div class="col-md-6 col-md-offset-3">
							<div class="section-title">
								<?php echo $service_block['header']; ?>
							</div>
						</div>
					</div>
					<div class="row">
						<?php foreach ( $service_block['item-list'] as $item ) :
							$item = $item['item'];
							$options = get_field( 'options', $item->ID );
							?>
							<div class="col-xs-12 col-sm-6 col-md-3">
								<div class="team-block">
									<div class="team-image">
										<?php echo get_the_post_thumbnail( $item, 'service-thumbnail' ); ?>
										<div class="team-figure">
											<div class="item-table">
												<div class="item-tablecell">
													<div class="text-box text-box-style2">
														<h5><?php echo $item->post_title; ?></h5>
														<p class="content-goods">
															<?php echo nl2br( $options ); ?>
														</p>
													</div>
													<a class="btn btn-submit" href="<?php the_permalink( $item ); ?>">Подробнее</a>
												</div>
											</div>
										</div>
									</div>
									<div class="team-text">
										<h5><?php echo $item->post_title; ?></h5>
										<p><?php echo get_field( 'short_options', $item->ID ); ?></p>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>

	<!-- News &amp; Testimonials Start -->
	<div class="wshipping-content-block news-testimonial-block">
		<div class="container wow fadeInUp">
			<div class="row">
				<!-- Latest News Start -->
				<div class="col-xs-12 col-sm-12 col-md-5">
					<h3 class="heading3-border text-uppercase">Новости компании</h3>
					<?php
					$args = array(
						'numberposts' => '2',
						'post_type'   => 'company_news',
						'post_status' => 'publish',
					);
					$news = get_posts( $args );
					$i    = 1;
					foreach ( $news as $post ) :
						setup_postdata( $post );
						$content = strip_shortcodes( $post->post_content );
						$excerpt = wp_trim_words( $content, $num_words = 12, $more = null );
						?>
						<div class="latest-news-section <?php if ( 1 === $i ): echo 'mt0'; endif; ?> wow fadeInUp">
							<div class="news-date">
								<?php echo get_the_date( 'd' ); ?><span><?php echo get_the_date( 'M' ); ?></span>
							</div>
							<h4><?php the_title(); ?></h4>
							<div class="news-post-by">By <span><?php the_author(); ?></span></div>
							<?php echo wpautop( $excerpt ); ?>
						</div>

						<?php
						$i ++;
					endforeach;
					wp_reset_postdata();
					?>
				</div>
				<!-- Latest News End -->

				<!-- Testimonial start -->
				<div class="col-xs-12 col-sm-12 col-md-7 home-testimonial">
					<h3 class="heading3-border text-uppercase">Как мы работаем</h3>
					<div class="testimonial">
						<?php
						$args  = array(
							'numberposts' => '-1',
							'post_type'   => 'how_we_work',
							'post_status' => 'publish',
						);
						$works = get_posts( $args );
						foreach ( $works as $work ) :
							setup_postdata( $work );
							?>
							<div class="testimonial-item">
								<div class="row">
									<div class="col-md-5">
										<div class="testimonial-img-bg" style="background-image: url('<?php echo get_the_post_thumbnail_url( $work ); ?>');"></div>
									</div>
									<div class="col-md-7">
										<div class="testimonial-content">
											<?php the_content(); ?>
										</div>
									</div>
								</div>
							</div>
						<?php
						endforeach;
						wp_reset_postdata(); ?>
					</div>
				</div>
				<!-- Testimonial end -->
			</div>
		</div>
	</div>
	<!-- News & Testimonials End -->

	<!-- Our client start -->
	<?php get_template_part( 'template-parts/block', 'clients' ); ?>
	<!-- Our client end -->

<?php endwhile;
wp_reset_postdata();
?>

<?php get_footer(); ?>
