<?php
/**
 * Rhea functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

if ( ! function_exists( 'rhea_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function rhea_setup() {
		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );
		/*
		 * This theme uses wp_nav_menu() in one location.
		 */
		register_nav_menus( array(
			'menu-1' => esc_html( 'Primary' ),
			'menu-2' => esc_html( 'Quick links' ),
			'menu-3' => esc_html( 'Blog sidebar menu' ),
			'menu-4' => esc_html( 'Products sidebar menu' )
		) );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'service-thumbnail', 270, 290, true );


		add_theme_support( 'custom-logo' );

//		update_option('image_default_align', 'center' );
//		update_option('image_default_size', 'large' );
	}
endif;

add_action( 'after_setup_theme', 'rhea_setup' );

if ( ! function_exists( 'rhea_enqueue_scripts_and_styles' ) ) :
	function rhea_enqueue_scripts_and_styles() {

		wp_enqueue_style(
			'bootstrap-style',
			get_template_directory_uri() . '/assets/css/bootstrap.min.css',
			array(),
			'1.0.0',
			'all'
		);

		wp_enqueue_style(
			'font-awesome',
			get_template_directory_uri() . '/assets/css/font-awesome.min.css',
			array(),
			'1.0.0',
			'all'
		);

		wp_enqueue_style(
			'animate-style',
			get_template_directory_uri() . '/assets/css/animate.min.css',
			array(),
			'1.0.0',
			'all'
		);

		wp_enqueue_style(
			'owl-carousel-style',
			get_template_directory_uri() . '/assets/css/owl.carousel.css',
			array(),
			'1.0.0',
			'all'
		);

		wp_enqueue_style(
			'magnific-popup-style',
			get_template_directory_uri() . '/assets/css/magnific-popup.css',
			array(),
			'1.0.0',
			'all'
		);

		wp_enqueue_style(
			'slicknav-style',
			get_template_directory_uri() . '/assets/css/slicknav.min.css',
			array(),
			'1.0.0',
			'all'
		);

		wp_enqueue_style(
			'bootstrap-datepicker-style',
			get_template_directory_uri() . '/assets/css/bootstrap-datepicker.min.css',
			array( 'bootstrap-style' ),
			'1.0.0',
			'all'
		);

		wp_enqueue_style(
			'rhea-theme-style',
			get_template_directory_uri() . '/theme.css',
			array( 'bootstrap-style' ),
			'1.0.0',
			'all'
		);

		wp_enqueue_style(
			'rhea-theme-responsive-style',
			get_template_directory_uri() . '/assets/css/responsive.css',
			array( 'rhea-theme-style' ),
			'1.0.0',
			'all'
		);

		wp_enqueue_style(
			'thea-style',
			get_stylesheet_uri(),
			array( 'rhea-theme-responsive-style' ),
			'1.0.0',
			'all'
		);

		wp_enqueue_script(
			'jquery2-js',
			get_template_directory_uri() . '/assets/js/jquery-2.1.3.min.js',
			array(),
			'2.1.3',
			false
		);

		wp_enqueue_script(
			'bootstrap-js',
			get_template_directory_uri() . '/assets/js/bootstrap.min.js',
			array(),
			'3.3.7',
			true
		);

		wp_enqueue_script(
			'bootstrap-datepicker',
			get_template_directory_uri() . '/assets/js/bootstrap-datepicker.min.js',
			array( 'bootstrap-js' ),
			'1.5.7',
			true
		);

		wp_enqueue_script(
			'owl-carousel-js',
			get_template_directory_uri() . '/assets/js/owl.carousel.min.js',
			array(),
			'',
			true
		);

		wp_enqueue_script(
			'magnific-popup-js',
			get_template_directory_uri() . '/assets/js/jquery.magnific-popup.min.js',
			array(),
			'1.0.1',
			true
		);

		wp_enqueue_script(
			'mixitup-js',
			get_template_directory_uri() . '/assets/js/jquery.mixitup.min.js',
			array(),
			'2.1.11',
			true
		);

		wp_enqueue_script(
			'wow-js',
			get_template_directory_uri() . '/assets/js/wow-1.3.0.min.js',
			array(),
			'1.3.0',
			true
		);

		wp_enqueue_script(
			'validate-js',
			get_template_directory_uri() . '/assets/js/jquery.validate.js',
			array(),
			'1.13.1',
			true
		);

		wp_enqueue_script(
			'form-step-js',
			get_template_directory_uri() . '/assets/js/form-step.js',
			array( 'validate-js' ),
			'1.13.1',
			true
		);

		wp_enqueue_script(
			'slicknav-js',
			get_template_directory_uri() . '/assets/js/jquery.slicknav.min.js',
			array(),
			'1.0.10',
			true
		);

		wp_enqueue_script(
			'waypoints-js',
			'https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js',
			array( 'jquery' ),
			'4.0.1',
			true
		);

		wp_enqueue_script(
			'active-js',
			get_template_directory_uri() . '/assets/js/active.js',
			array( 'waypoints-js' ),
			'1.0.0',
			true
		);

	}
endif;

add_action( 'wp_enqueue_scripts', 'rhea_enqueue_scripts_and_styles' );

/**
 * Retrieve paginated link for archive post pages.
 *
 * Technically, the function can be used to create paginated link list for any
 * area. The 'base' argument is used to reference the url, which will be used to
 * create the paginated links. The 'format' argument is then used for replacing
 * the page number. It is however, most likely and by default, to be used on the
 * archive post pages.
 *
 * The 'type' argument controls format of the returned value. The default is
 * 'plain', which is just a string with the links separated by a newline
 * character. The other possible values are either 'array' or 'list'. The
 * 'array' value will return an array of the paginated link list to offer full
 * control of display. The 'list' value will place all of the paginated links in
 * an unordered HTML list.
 *
 * The 'total' argument is the total amount of pages and is an integer. The
 * 'current' argument is the current page number and is also an integer.
 *
 * An example of the 'base' argument is "http://example.com/all_posts.php%_%"
 * and the '%_%' is required. The '%_%' will be replaced by the contents of in
 * the 'format' argument. An example for the 'format' argument is "?page=%#%"
 * and the '%#%' is also required. The '%#%' will be replaced with the page
 * number.
 *
 * You can include the previous and next links in the list by setting the
 * 'prev_next' argument to true, which it is by default. You can set the
 * previous text, by using the 'prev_text' argument. You can set the next text
 * by setting the 'next_text' argument.
 *
 * If the 'show_all' argument is set to true, then it will show all of the pages
 * instead of a short list of the pages near the current page. By default, the
 * 'show_all' is set to false and controlled by the 'end_size' and 'mid_size'
 * arguments. The 'end_size' argument is how many numbers on either the start
 * and the end list edges, by default is 1. The 'mid_size' argument is how many
 * numbers to either side of current page, but not including current page.
 *
 * It is possible to add query vars to the link by using the 'add_args' argument
 * and see add_query_arg() for more information.
 *
 * The 'before_page_number' and 'after_page_number' arguments allow users to
 * augment the links themselves. Typically this might be to add context to the
 * numbered links so that screen reader users understand what the links are for.
 * The text strings are added before and after the page number - within the
 * anchor tag.
 *
 * @since 2.1.0
 *
 * @global WP_Query    $wp_query
 * @global WP_Rewrite  $wp_rewrite
 *
 * @param string|array $args {
 *     Optional. Array or string of arguments for generating paginated links for archives.
 *
 * @type string        $base Base of the paginated url. Default empty.
 * @type string        $format Format for the pagination structure. Default empty.
 * @type int           $total The total amount of pages. Default is the value WP_Query's
 *                                      `max_num_pages` or 1.
 * @type int           $current The current page number. Default is 'paged' query var or 1.
 * @type bool          $show_all Whether to show all pages. Default false.
 * @type int           $end_size How many numbers on either the start and the end list edges.
 *                                      Default 1.
 * @type int           $mid_size How many numbers to either side of the current pages. Default 2.
 * @type bool          $prev_next Whether to include the previous and next links in the list. Default true.
 * @type bool          $prev_text The previous page text. Default '&laquo; Previous'.
 * @type bool          $next_text The next page text. Default 'Next &raquo;'.
 * @type string        $type Controls format of the returned value. Possible values are 'plain',
 *                                      'array' and 'list'. Default is 'plain'.
 * @type array         $add_args An array of query args to add. Default false.
 * @type string        $add_fragment A string to append to each link. Default empty.
 * @type string        $before_page_number A string to appear before the page number. Default empty.
 * @type string        $after_page_number A string to append after the page number. Default empty.
 * }
 * @return array|string|void String of page links or array of page links.
 */
function rhea_paginate_links( $args = '' ) {
	global $wp_query, $wp_rewrite;

	// Setting up default values based on the current URL.
	$pagenum_link = html_entity_decode( get_pagenum_link() );
	$url_parts    = explode( '?', $pagenum_link );

	// Get max pages and current page out of the current query, if available.
	$total   = isset( $wp_query->max_num_pages ) ? $wp_query->max_num_pages : 1;
	$current = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;

	// Append the format placeholder to the base URL.
	$pagenum_link = trailingslashit( $url_parts[0] ) . '%_%';

	// URL base depends on permalink settings.
	$format = $wp_rewrite->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
	$format .= $wp_rewrite->using_permalinks() ? user_trailingslashit( $wp_rewrite->pagination_base . '/%#%', 'paged' ) : '?paged=%#%';

	$defaults = array(
		'base'               => $pagenum_link,
		// http://example.com/all_posts.php%_% : %_% is replaced by format (below)
		'format'             => $format,
		// ?page=%#% : %#% is replaced by the page number
		'total'              => $total,
		'current'            => $current,
		'show_all'           => false,
		'prev_next'          => true,
		'prev_text'          => __( '&laquo; Previous' ),
		'next_text'          => __( 'Next &raquo;' ),
		'end_size'           => 1,
		'mid_size'           => 2,
		'type'               => 'plain',
		'add_args'           => array(),
		// array of query args to add
		'add_fragment'       => '',
		'before_page_number' => '',
		'after_page_number'  => ''
	);

	$args = wp_parse_args( $args, $defaults );

	if ( ! is_array( $args['add_args'] ) ) {
		$args['add_args'] = array();
	}

	// Merge additional query vars found in the original URL into 'add_args' array.
	if ( isset( $url_parts[1] ) ) {
		// Find the format argument.
		$format       = explode( '?', str_replace( '%_%', $args['format'], $args['base'] ) );
		$format_query = isset( $format[1] ) ? $format[1] : '';
		wp_parse_str( $format_query, $format_args );

		// Find the query args of the requested URL.
		wp_parse_str( $url_parts[1], $url_query_args );

		// Remove the format argument from the array of query arguments, to avoid overwriting custom format.
		foreach ( $format_args as $format_arg => $format_arg_value ) {
			unset( $url_query_args[ $format_arg ] );
		}

		$args['add_args'] = array_merge( $args['add_args'], urlencode_deep( $url_query_args ) );
	}

	// Who knows what else people pass in $args
	$total = (int) $args['total'];
	if ( $total < 2 ) {
		return;
	}
	$current  = (int) $args['current'];
	$end_size = (int) $args['end_size']; // Out of bounds?  Make it the default.
	if ( $end_size < 1 ) {
		$end_size = 1;
	}
	$mid_size = (int) $args['mid_size'];
	if ( $mid_size < 0 ) {
		$mid_size = 2;
	}
	$add_args   = $args['add_args'];
	$r          = '';
	$page_links = array();
	$dots       = false;

	if ( $args['prev_next'] && $current && 1 < $current ) :
		$link = str_replace( '%_%', 2 == $current ? '' : $args['format'], $args['base'] );
		$link = str_replace( '%#%', $current - 1, $link );
		if ( $add_args ) {
			$link = add_query_arg( $add_args, $link );
		}
		$link .= $args['add_fragment'];

		/**
		 * Filters the paginated links for the given archive pages.
		 *
		 * @since 3.0.0
		 *
		 * @param string $link The paginated link URL.
		 */
		$page_links[] = '<a class="prev page-numbers" aria-label="Previous" href="' . esc_url( apply_filters( 'paginate_links', $link ) ) . '">' . $args['prev_text'] . '</a>';
	endif;
	for ( $n = 1; $n <= $total; $n ++ ) :
		if ( $n == $current ) :
			$page_links[] = "<span class='page-numbers current pagin-active' >" . $args['before_page_number'] . number_format_i18n( $n ) . $args['after_page_number'] . "</span>";
			$dots         = true;
		else :
			if ( $args['show_all'] || ( $n <= $end_size || ( $current && $n >= $current - $mid_size && $n <= $current + $mid_size ) || $n > $total - $end_size ) ) :
				$link = str_replace( '%_%', 1 == $n ? '' : $args['format'], $args['base'] );
				$link = str_replace( '%#%', $n, $link );
				if ( $add_args ) {
					$link = add_query_arg( $add_args, $link );
				}
				$link .= $args['add_fragment'];

				/** This filter is documented in wp-includes/general-template.php */
				$page_links[] = "<a class='page-numbers' href='" . esc_url( apply_filters( 'paginate_links', $link ) ) . "'>" . $args['before_page_number'] . number_format_i18n( $n ) . $args['after_page_number'] . "</a>";
				$dots         = true;
			elseif ( $dots && ! $args['show_all'] ) :
				$page_links[] = '<span class="page-numbers dots">' . __( '&hellip;' ) . '</span>';
				$dots         = false;
			endif;
		endif;
	endfor;
	if ( $args['prev_next'] && $current && $current < $total ) :
		$link = str_replace( '%_%', $args['format'], $args['base'] );
		$link = str_replace( '%#%', $current + 1, $link );
		if ( $add_args ) {
			$link = add_query_arg( $add_args, $link );
		}
		$link .= $args['add_fragment'];

		/** This filter is documented in wp-includes/general-template.php */
		$page_links[] = '<a class="next page-numbers" aria-label="Next" href="' . esc_url( apply_filters( 'paginate_links', $link ) ) . '">' . $args['next_text'] . '</a>';
	endif;
	switch ( $args['type'] ) {
		case 'array' :
			return $page_links;

		case 'list' :
			$r .= "<ul class='pagination'>\n\t<li>";
			$r .= join( "</li>\n\t<li>", $page_links );
			$r .= "</li>\n</ul>\n";
			break;

		default :
			$r = join( "\n", $page_links );
			break;
	}

	return $r;
}

/**
 * Displays a paginated navigation to next/previous set of posts, when applicable.
 *
 * @since 4.1.0
 *
 * @param array $args Optional. See get_the_posts_pagination() for available arguments.
 *                    Default empty array.
 */
function rhea_the_posts_pagination( $args = array() ) {
	echo rhea_get_the_posts_pagination( $args );
}

/**
 * Retrieves a paginated navigation to next/previous set of posts, when applicable.
 *
 * @since 4.1.0
 *
 * @param array $args {
 *     Optional. Default pagination arguments, see paginate_links().
 *
 * @type string $screen_reader_text Screen reader text for navigation element.
 *                                      Default 'Posts navigation'.
 * }
 * @return string Markup for pagination links.
 */
function rhea_get_the_posts_pagination( $args = array() ) {
	$navigation = '';

	// Don't print empty markup if there's only one page.
	if ( $GLOBALS['wp_query']->max_num_pages > 1 ) {
		$args = wp_parse_args( $args, array(
			'mid_size'           => 1,
			'prev_text'          => _x( 'Previous', 'previous set of posts' ),
			'next_text'          => _x( 'Next', 'next set of posts' ),
			'screen_reader_text' => __( 'Posts navigation' ),
		) );

		// Make sure we get a string back. Plain is the next best thing.
		if ( isset( $args['type'] ) && 'array' == $args['type'] ) {
			$args['type'] = 'plain';
		}

		// Set up paginated links.
		$links = rhea_paginate_links( $args );

		if ( $links ) {
			$navigation = _navigation_markup( $links, 'page-navigation', $args['screen_reader_text'] );
		}
	}

	return $navigation;
}

// удаляет H2 из шаблона пагинации
add_filter( 'navigation_markup_template', 'rhea_navigation_template', 10, 2 );
function rhea_navigation_template( $template, $class ) {
	/*
	Вид базового шаблона:
	<nav class="navigation %1$s" role="navigation">
		<h2 class="screen-reader-text">%2$s</h2>
		<div class="nav-links">%3$s</div>
	</nav>
	*/

	return '
	<nav class="navigation %1$s" role="navigation">
		<div class="nav-links">%3$s</div>
	</nav>    
	';
}

function rhea_get_sidebar( $name = '' ) {
	do_action( 'get_sidebar', $name );

	if ( $name ) {
		$name = "-$name";
	}

	locate_template( "template-parts/sidebar$name.php", true );
}

if ( function_exists( 'acf_add_options_page' ) ) {

	acf_add_options_page( array(
		'page_title' => 'Theme General Settings',
		'menu_title' => 'Theme Settings',
		'menu_slug'  => 'theme-general-settings',
		'capability' => 'edit_posts',
		'redirect'   => false
	) );
}

/**
 * Меняет порядок вывода полей в форме комментов
 *
 * @param $fields
 *
 * @return array
 */
function rhea_reorder_comment_fields( $fields ) {
	$new_fields = array();

	$myorder = array( 'author', 'email', 'url', 'comment' );
	foreach ( $myorder as $key ) {
		$new_fields[ $key ] = $fields[ $key ];
		unset( $fields[ $key ] );
	}

	if ( $fields ) {
		foreach ( $fields as $key => $val ) {
			$new_fields[ $key ] = $val;
		}
	}

	return $new_fields;
}

add_filter( 'comment_form_fields', 'rhea_reorder_comment_fields' );

/**
 * Верстка комментов
 *
 * @param $comment
 * @param $args
 * @param $depth
 */
function rhea_custom_comments( $comment, $args, $depth ) {
	$GLOBALS['comment']       = $comment;
	$GLOBALS['comment_depth'] = $depth;
	$comment_class            = '';
	$comment_status           = 'replay-comment';
	if ( $depth == 1 ) :
		$comment_class  = 'comment-group';
		$comment_status = 'comment-content';

	endif;
	?>

<div id="comment-wrapper-<?php comment_ID(); ?>" <?php comment_class( $comment_class ); ?>>
	<article id="comment-<?php comment_ID(); ?>">
		<div class="<?php echo $comment_status; ?>">
			<h6><?php comment_author(); ?> <span><i class="fa fa-clock-o"></i> <a
							href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf( '%1$s at %2$s', get_comment_date( 'F d, Y' ), get_comment_time() ) ?></a></span>
			</h6>
			<p><?php comment_text(); ?></p>
			<?php
			$user          = wp_get_current_user();
			$allowed_roles = array( 'editor', 'administrator', 'author' );
			if ( array_intersect( $allowed_roles, $user->roles ) ) : ?>
				<?php comment_reply_link( array_merge( $args, array(
					'depth'     => $depth,
					'max_depth' => $args['max_depth']
				) ) ) ?>
			<?php endif; ?>
		</div>
	</article>
	<?php
}

function rhea_get_youtube_video_id( $video ) {
	preg_match( '/src="(.+?)"/', $video, $matches_url );
	$src = $matches_url[1];

	preg_match( '/embed(.*?)?feature/', $src, $matches_id );
	$id = $matches_id[1];
	$id = str_replace( str_split( '?/' ), '', $id );

	return $id;
}

function rhea_get_youtube_video_thumbnail_url($video) {
	$id = rhea_get_youtube_video_id($video);
	return "https://img.youtube.com/vi/{$id}/mqdefault.jpg";
}

require 'inc/helpers.php';

require 'inc/custom-post-types.php';

require 'inc/shortcodes.php';