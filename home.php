<?php get_header();
the_post();
?>

<?php get_template_part('template-parts/block', 'breadcrumbs'); ?>

<!-- Blog content start -->
<div class="wshipping-content-block">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-9 pull-right">
				<?php
				if ( have_posts() ) :
					while ( have_posts() ) : the_post();
						get_template_part( 'template-parts/content', 'post-preview' );
					endwhile;
					rhea_the_posts_pagination( array(
						'type'      => 'list',
						'next_text' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
						'prev_text' => '<i class="fa fa-angle-left" aria-hidden="true"></i>'
					) );
					wp_reset_postdata();
				endif; ?>
			</div>
			<!-- Blog sidebar start -->
			<div class="col-xs-12 col-sm-12 col-md-3">
				<?php rhea_get_sidebar( 'blog' ); ?>
			</div>
			<!-- Blog sidebar end -->
		</div>
	</div>
</div>
<!-- Blog content end -->

<?php get_footer(); ?>
