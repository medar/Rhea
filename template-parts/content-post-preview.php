<?php

$content  = strip_shortcodes( $post->post_content );
$excerpt  = wp_trim_words( $content, $num_words = 30, $more = null );

?>
<div class="blog-block">
	<div class="blog-image-bg picture__wrapper">
		<picture>
			<?php the_post_thumbnail(); ?>
		</picture>
	</div>
	<div class="blog-content">
		<div class="blog-date"><?php echo get_the_date('M'); ?> <span><?php echo get_the_date('d'); ?></span> <?php echo get_the_date('Y'); ?></div>
		<h3><a href="<?php the_permalink(); ?>" title=""><?php the_title(); ?></a></h3>
		<?php echo wpautop( $excerpt ); ?>
	</div>
	<div class="continue-read-blog"><a href="<?php the_permalink(); ?>" title="">Continue Reading <i
					class="fa fa-angle-right"></i></a></div>
</div>
