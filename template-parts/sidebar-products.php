<div class="left-block left-menu">
	<?php
	wp_nav_menu(
		array(
			'theme_location'  => 'menu-4',
			'menu_id'         => '',
			'container'       => '',
			'container_class' => '',
		)
	);
	?>
</div>
<div class="left-block wow fadeInUp">
	<h3 class="heading3-border text-uppercase">Новости</h3>
	<?php
	$args = array(
		'numberposts' => '2',
		'post_type'   => 'company_news',
		'post_status' => 'publish',
	);
	$news = get_posts( $args );
	foreach ( $news as $post ) :
		setup_postdata( $post );
		$content = strip_shortcodes( $post->post_content );
		$excerpt = wp_trim_words( $content, $num_words = 12, $more = null );
		?>
		<div class="latest-news-section">
			<div class="news-date"><?php echo get_the_date( 'd' ); ?><span><?php echo get_the_date( 'M' ); ?></span>
			</div>
			<h4><?php the_title(); ?></h4>
			<div class="news-post-by">By <span><?php the_author(); ?></span></div>
			<?php echo wpautop( $excerpt ); ?>
		</div>
	<?php
	endforeach;
	wp_reset_postdata();
	?>
</div>

<div class="left-block wow fadeInUp">
	<h3 class="heading3-border text-uppercase">Как мы работаем</h3>
	<div class="testimonial">
		<?php
		$args  = array(
			'numberposts' => '-1',
			'post_type'   => 'how_we_work',
			'post_status' => 'publish',
		);
		$works = get_posts( $args );
		foreach ( $works as $work ) :
		setup_postdata($work);
		?>
		<div class="testimonial-item">
			<div class="row">
				<div class="col-md-12">
					<div class="testimonial-img-bg" style="background-image: url('<?php echo get_the_post_thumbnail_url($work); ?>');"></div>
				</div>
				<div class="col-md-12">
					<div class="testimonial-content">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</div>
		<?php
		endforeach;
		wp_reset_postdata(); ?>
	</div>
</div>
<div class="left-block wow fadeInUp affix-quote">

	<div class="right-quote-from">
		<?php echo do_shortcode('[contact-form-7 id="216" title="Заказать просчет сайдбар"]'); ?>
	</div>

</div>