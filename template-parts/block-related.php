<?php if( get_field('related_items') ) :  ?>
<div class="you-may-like container-fluid">
	<h3 class="heading3-border text-uppercase">Вам может быть интересно</h3>
	<div class="row equal">
		<?php foreach ( get_field( 'related_items' ) as $item ) : setup_postdata( $item['item'] ); ?>
			<?php $item = $item['item']; ?>
			<div class="col-xs-12 col-sm-6 col-md-6">
				<div class="blog-item wow fadeInUp">
					<a href="<?php the_permalink($item); ?>" title="">
						<div class="blog-item-bg" style="background-image: url(<?php echo get_the_post_thumbnail_url($item); ?>);"></div>
						<div class="blog-content">
							<h3><?php echo get_the_title($item); ?></h3>
							<h6>Опубликовано <?php echo get_the_date('d F Y', $item); ?> автором
								<?php the_author(); ?></h6>
						</div>
					</a>
				</div>
			</div>
		<?php endforeach;
		wp_reset_postdata();
		?>
	</div>
</div>
<?php endif; ?>