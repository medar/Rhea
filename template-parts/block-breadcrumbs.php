<div class="wshipping-content-block wshipping-breadcroumb inner-bg-1">
	<div class="container">
		<div class="row">
			<div class="col-md-7">
				<h2><?php if(is_home()) : echo "Блог"; else : the_title(); endif; ?></h2>
				<?php
				if ( function_exists( 'yoast_breadcrumb' ) ) {
					yoast_breadcrumb( '<p id="breadcrumbs">', '</p>' );
				}
				?>
			</div>
		</div>
	</div>
</div>
