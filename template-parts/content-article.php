<div class="single-blog-details">
	<div class="picture__wrapper">
		<picture>
			<?php the_post_thumbnail( array( 776, 465 ), array( 'class' => '' ) ); ?>
		</picture>
	</div>
	<div class="single-blog-content">
		<h2><?php the_title(); ?></h2>
		<h6>Posted on <?php the_date('d M Y'); ?> by <?php the_author(); ?></h6>
		<?php the_content(); ?>
	</div>
</div>
