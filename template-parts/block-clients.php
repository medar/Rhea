<div class="wshipping-content-block client-block pt0">
	<div class="container">
		<div class="row">
			<div class="col-md-12"><h3 class="heading3-border text-uppercase">Наши клиенты</h3></div>
			<div class="our-client">
				<?php $clients_gallery = get_field('clients_gallery', 'option');
				foreach ($clients_gallery as $client) :
				?>
				<div class="client-item">
					<img src="<?php echo $client['url']; ?>" alt="<?php if (!empty($client['alt'])) : echo $client['alt']; else : echo $client['title']; endif; ?>"/>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>
