<div class="left-block left-menu menu-with-title">
	<?php
	wp_nav_menu(
		array(
			'theme_location'  => 'menu-3',
			'menu_id'         => '',
			'container'       => '',
			'container_class' => '',
		)
	);
	?>
</div>
<?php

$page_id = get_option('page_for_posts');
$tag_links = get_field('tag_links', $page_id);
if(!empty($tag_links)) : ?>
	<div class="left-block tags">
		<h3 class="heading3-border text-uppercase"><?php the_field('tags_header', $page_id); ?></h3>
		<?php foreach ($tag_links as $tag_link) : ?>
			<a href="<?php echo $tag_link['link_href']; ?>" title="<?php echo $tag_link['link_text']; ?>"><?php echo $tag_link['link_text']; ?></a>
		<?php endforeach; ?>
	</div>
<?php endif; ?>
