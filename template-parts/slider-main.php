<?php
$slider = get_field( 'slider' );
if (!empty($slider)) :
?>

<div class="homepage-slides-wrapper">
	<div class="homepage-slides text-center">
		<?php
		foreach ( $slider as $slide ) : ?>
			<div class="single-slide-item" style="background-image: url(<?php echo $slide['img']; ?>)">
				<div class="item-table">
					<div class="item-tablecell">
						<div class="container">
							<div class="row">
								<div class="col-md-10 col-md-offset-1">
									<h3><?php echo $slide['header_1']; ?></h3>
									<h1><?php echo $slide['header_2']; ?></h1>
									<?php echo wpautop($slide['content']); ?>
									<a href="<?php echo $slide['btn_link']; ?>" class="wshipping-button slide-btn"><?php echo $slide['btn_text']; ?></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</div>
<?php endif; ?>