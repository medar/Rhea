<div class="right-block left-menu menu-with-title">
	<?php
	wp_nav_menu(
		array(
			'theme_location'  => 'menu-3',
			'menu_id'         => '',
			'container'       => '',
			'container_class' => '',
		)
	);
	?>
</div>
<div class="right-block letest-post">
	<h4 class="title-with-bg bg-black">Latest post</h4>
	<ul>
		<?php
		$args  = array(
			'numberposts' => '4',
			'post_type'   => 'post',
			'post_status' => 'publish',
		);
		$posts = get_posts( $args );
		foreach ( $posts as $post ) :
			setup_postdata( $post );
			$content = strip_shortcodes( $post->post_content );
			$excerpt = wp_trim_words( $content, $num_words = 12, $more = null );
			?>
			<li>
				<a href="<?php the_permalink(); ?>" title="<?php the_title();?>">
					<?php the_post_thumbnail(array(80,65)); ?>
					<p><?php the_title(); ?></p>
					<span><?php echo get_the_date('d F Y'); ?></span>
				</a>
			</li>
		<?php
		endforeach;
		wp_reset_postdata();
		?>
	</ul>
</div>
<?php
$tag_links = get_field('tag_links');
if(!empty($tag_links)) : ?>
<div class="left-block tags">
	<h3 class="heading3-border text-uppercase"><?php the_field('tags_header'); ?></h3>
	<?php foreach ($tag_links as $tag_link) : ?>
		<a href="<?php echo $tag_link['link_href']; ?>" title="<?php echo $tag_link['link_text']; ?>"><?php echo $tag_link['link_text']; ?></a>
	<?php endforeach; ?>
</div>
<?php endif; ?>