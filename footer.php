<!-- Footer start -->
<footer class="site-footer">
	<!-- Footer Top start -->
	<div class="footer-top-area wow fadeInUp">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="footer-wiz">
						<h3 class="footer-logo"><img
									src="<?php echo get_template_directory_uri(); ?>/assets/images/footer-logo.png"
									alt="footer logo"/></h3>
						<p><?php echo get_field( 'footer', 'option' )['logo_text']; ?></p>
						<ul class="footer-contact">
							<?php $links = get_field( 'footer', 'option' )['links']; ?>
							<?php foreach ( $links as $link ) : ?>
								<li>
									<?php echo $link['icon']; ?>
									<a href="<?php echo esc_attr( $link['href'] ); ?>"><?php echo $link['title']; ?></a>
								</li>
							<?php endforeach; ?>
						</ul>
					</div>
					<div class="top-social bottom-social">
						<?php $social = get_field( 'footer', 'option' )['social']; ?>
						<?php foreach ( $social as $item ) : ?>
							<a href="<?php echo $item['link']; ?>" target="_blank"><?php echo $item['icon']; ?></a>
						<?php endforeach; ?>

					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="footer-wiz footer-menu">
						<h3 class="footer-wiz-title">Quick Links</h3>
						<?php
						wp_nav_menu(
							array(
								'theme_location'  => 'menu-2',
								'menu_id'         => '',
								'container'       => '',
								'container_class' => '',
							)
						);
						?>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="footer-wiz footer-menu">
						<h3 class="footer-wiz-title">Usefull Links</h3>
						<!-- Single service Gallery start -->
						<div class="single-service-galler">
							<div class="row equal-full">
								<?php $gallery = get_field( 'footer', 'option' )['gallery']; ?>
								<?php foreach ( $gallery as $img ) : ?>
									<div class="col-xs-12 col-sm-6 col-md-6 l-gallery-holder">
										<a class="latest-gallery__footer" href="<?php echo $img['url']; ?>"
										   title="<?php if ( ! empty( $img['alt'] ) ) : echo $img['alt']; else : echo $img['title']; endif; ?>"
										   data-effect="fadeIn">
											<i class="fa fa-search-plus" aria-hidden="true"></i>
											<img src="<?php echo $img['url']; ?>"
												 alt="<?php if ( ! empty( $img['alt'] ) ) : echo $img['alt']; else : echo $img['title']; endif; ?>"/>
										</a>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
						<!-- Single service gallery end -->
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-3">
					<div class="footer-wiz">
						<h3 class="footer-wiz-title"><?php echo get_field( 'footer', 'option' )['contact_title']; ?></h3>
						<ul class="open-hours">
							<?php foreach ( get_field( 'footer', 'option' )['contacts'] as $item ) : ?>
								<li><span><?php echo esc_html( $item['contact_item'] ); ?></span></li>
							<?php endforeach; ?>
						</ul>
						<!--						<div class="newsletter">-->
						<!--							<form action="about.html">-->
						<!--								<input type="text" placeholder="Введите Ваш е-mail" value="" class="news-input">-->
						<!--								<button type="submit" value="" class="news-btn"><i class="fa fa-location-arrow"></i>-->
						<!--								</button>-->
						<!--							</form>-->
						<!--						</div>-->
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- footer top end -->

	<!-- copyright start -->
	<div class="footer-bottom-area">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-md-6"><?php echo get_field( 'footer', 'option' )['cop_left']; ?></div>
				<div class="col-md-12 col-md-6 text-right"><?php echo get_field( 'footer', 'option' )['cop_right']; ?></div>
			</div>
		</div>
	</div>
	<!-- copyright end -->
</footer>
<!-- Footer end -->
<div id="contact__form--popup" class="white-popup-block mfp-hide">
		<?php echo do_shortcode( '[contact-form-7 id="574" title="Заказать просчет всплывающая"]' ); ?>
</div>

<!-- Start scroll top -->
<div class="scrollup"><i class="fa fa-angle-up"></i></div>
<!-- End scroll top -->

<?php wp_footer(); ?>
</body>
</html>