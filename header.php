<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<header>
	<!-- Header top area start -->
	<div class="header-top-area">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="top-contact">
						<?php $contacts = get_field('main_header' , 'option'); ?>
						<a href="mailto:<?php echo $contacts['mail']; ?>"><i class="fa fa-envelope"></i> <?php echo $contacts['mail']; ?></a>
						<a href="tel:<?php echo $contacts['phone']; ?>"><i class="fa fa-phone"></i><?php echo $contacts['phone']; ?></a>
						<a href="#"><i class="fa fa-clock-o"></i> <?php echo $contacts['time']; ?></a>
					</div>
				</div>

			</div>
		</div>
	</div>
	<!-- Header top area End -->

	<!-- Header area start -->
	<div class="header-area">
		<div class="container">
			<!-- Site logo Start -->
			<div class="logo">
					<h1>
					<?php the_custom_logo();?>
					</h1>
			</div>
			<!-- Site logo end -->
			<div class="mobile-menu-wrapper"></div>

			<!-- Main menu start -->
			<?php
			wp_nav_menu(
				array(
					'theme_location'  => 'menu-1',
					'menu_id'         => 'navigation',
					'container'       => 'nav',
					'container_class' => 'mainmenu',
				)
			);
			?>
			<!-- Main menu end -->
		</div>
	</div>
	<!-- Header area End -->
</header>