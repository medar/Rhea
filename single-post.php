<?php get_header();

while ( have_posts() ) :
	the_post();
	?>

	<div <?php post_class(); ?>>

		<!-- Breadcroumbs start -->
		<div class="wshipping-content-block wshipping-breadcroumb inner-bg-1">
			<div class="container">
				<div class="row">
					<div class="col-md-7">
						<h2>Blog</h2>
						<?php
						if ( function_exists( 'yoast_breadcrumb' ) ) {
							yoast_breadcrumb( '<p id="breadcrumbs">', '</p>' );
						}
						?>
					</div>
				</div>
			</div>
		</div>
		<!-- Breadcroumbs end -->

		<!-- Blog Details start -->
		<div class="wshipping-content-block">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-8">
						<!-- Blog content start -->
						<?php get_template_part( 'template-parts/content', 'article' ); ?>
						<!-- Blog content end -->

						<div class="space60"></div>

						<?php get_template_part( 'template-parts/block', 'related' ); ?>

						<?php comments_template(); ?>

					</div>

					<!-- Right sidebar start -->
					<div class="col-xs-12 col-sm-12 col-md-4">
						<?php rhea_get_sidebar( 'post' ); ?>

					</div>
					<!-- Right sidebar end -->
				</div>
			</div>
		</div>
		<!-- Blog Details end -->

	</div>

<?php endwhile;
wp_reset_postdata();
?>

<?php get_footer(); ?>
