<?php get_header();

while ( have_posts() ) :
	the_post();
	?>

	<!-- Breadcroumbs start -->
	<div class="wshipping-content-block wshipping-breadcroumb inner-bg-1">
		<div class="container">
			<div class="row">
				<div class="col-md-7">
					<p class="text-uppercase">Our Services</p>
					<?php
					if ( function_exists( 'yoast_breadcrumb' ) ) {
						yoast_breadcrumb( '<p id="breadcrumbs">', '</p>' );
					}
					?>
				</div>
			</div>
		</div>
	</div>
	<!-- Breadcroumbs end -->

	<!-- Single service start -->
	<div class="wshipping-content-block">
		<div class="container">
			<div class="row">
				<div class="col-md-12"><h1 class="text-uppercase mt0"><?php the_title(); ?></h1></div>
			</div>
			<div class="row">
				<!-- Single service Gallery start -->
				<div class="col-xs-12 col-sm-12 col-md-6 single-service-galler rhea-service">
					<?php $gallery = get_field('gallery'); ?>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12 l-gallery-holder">
							<a class="latest-gallery"
							   href="<?php echo $gallery[0]['url']; ?>"
							   title="<?php echo $gallery[0]['title']; ?>" data-effect="fadeIn">
								<i class="fa fa-search-plus" aria-hidden="true"></i>
								<?php echo wp_get_attachment_image($gallery[0]['ID'], 'large'); ?>
							</a>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 portfolio-holder">
							<a class="latest-gallery"
							   href="<?php echo $gallery[1]['url']; ?>"
							   title="<?php echo $gallery[1]['title']; ?>" data-effect="zoomIn">
								<i class="fa fa-search-plus" aria-hidden="true"></i>
								<?php echo wp_get_attachment_image($gallery[1]['ID'], 'large'); ?>
							</a>
						</div>
						<div class="col-xs-6 col-sm-6 col-md-6 portfolio-holder">
							<a class="latest-gallery"
							   href="<?php echo $gallery[2]['url']; ?>"
							   title="<?php echo $gallery[2]['title']; ?>" data-effect="zoomIn">
								<i class="fa fa-search-plus" aria-hidden="true"></i>
								<?php echo wp_get_attachment_image($gallery[2]['ID'], 'large'); ?>
							</a>
						</div>
					</div>
				</div>
				<!-- Single service gallery end -->

				<!-- Advantages start -->
				<div class="col-xs-12 col-sm-12 col-md-5 col-md-offset-1">
					<?php $advantages = get_field('advantages'); ?>
					<h3 class="heading3-border text-uppercase"><?php echo $advantages['header'] ?></h3>
					<div class="advantages">
						<ul>
							<?php foreach ($advantages['items'] as $item) : ?>
							<li>
								<?php echo $item['icon']; ?>
								<h5><?php echo $item['header']; ?></h5>
								<p><?php echo $item['content']; ?></p>
							</li>
							<?php endforeach; ?>
						</ul>
					</div>
				</div>
				<!-- Advantages end -->
			</div>
			<div class="space40"></div>
			<div class="row">
				<!-- single service content start -->
				<div class="col-md-7">
					<div class="download-pdf">
						<?php $btns = get_field('btns'); ?>
						<?php foreach ($btns as $btn) : ?>
						<a href="<?php echo $btn['link'] ?>" title="Загрузка файла" class="wshipping-button download-btn">
							<?php echo $btn['icon']; ?>
							<?php echo $btn['title']; ?>
						</a>
						<?php endforeach; ?>
					</div>
					<?php the_content(); ?>
				</div>
				<!-- single service content end -->

				<!-- Get quote start -->
				<div class="col-md-5">
					<div class="right-quote-from">

						<?php echo do_shortcode('[contact-form-7 id="268" title="Заказать просчет страница servises"]'); ?>
					</div>
				</div>
				<!-- Get quote end -->
			</div>
		</div>
	</div>
	<!-- Single service end -->

	<?php get_template_part( 'template-parts/block', 'testimonials' ); ?>

	<!-- Our client start -->
	<?php get_template_part( 'template-parts/block', 'clients' ); ?>
	<!-- Our client end -->

<?php endwhile;
wp_reset_postdata();
?>

<?php get_footer(); ?>
