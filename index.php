<?php get_header();
$frontpage_id = get_option( 'page_on_front' );
?>


	<!-- Slider Start -->
<?php get_template_part( 'template-parts/slider', 'main' ); ?>
	<!-- Slider End -->

	<!-- Service start -->
	<div id="index-our-production" class="wshipping-content-block">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="section-title wow fadeInUp">
						<?php the_field( 'our_products_text' ); ?>
					</div>
				</div>
			</div>
			<div class="row equal">
				<?php
				$args = array(
					'numberposts' => '-1',
					'post_type'   => 'our_products',
					'post_status' => 'publish',
				);
				$news = get_posts( $args );
				foreach ( $news as $post ) :
					setup_postdata( $post );
					$content = strip_shortcodes( $post->post_content );
					$excerpt = wp_trim_words( $content, $num_words = 12, $more = null );
					?>
					<div class="col-xs-12 col-sm-12 col-md-4">
						<div class="single-service-item wow fadeInUp">
							<div class="service-item-bg"
								 style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"></a></div>
							<div class="service-content">
								<h4>
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
										<?php the_title(); ?>
									</a>
								</h4>
								<?php echo wpautop( $excerpt ); ?>
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="readmore-btn">Далее
									<i class="fa fa-angle-right"></i>
								</a>
								<div class="location_map">
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
										<i class="fa fa-plus"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
				<?php
				endforeach;
				wp_reset_postdata();

				?>
			</div>
		</div>
	</div>
	<!-- Service End -->

	<!-- we peovide start -->
<?php $we_offer = get_field( 'we_offer' ); ?>
	<div class="wshipping-content-block provided-block text-center"
		 style="background-image: url('<?php echo $we_offer['bg']; ?>')">
		<div class="item-table">
			<div class="item-tablecell">
				<div class="container">
					<div class="row">
						<div class="col-md-12 wow fadeInUp">
							<?php echo $we_offer['content']; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- we provide end -->

	<!-- Why Choose start -->
	<div class="wshipping-content-block home2-why-choose">
		<?php
		$who_choose_us = get_field( 'why_choose_us' );
		?>
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-4 wow fadeInLeft">
					<div class="picture__wrapper">
						<picture>
							<?php
							$img_id = $who_choose_us['why_choose_us_img'];
							echo wp_get_attachment_image( $img_id, 'large' ); ?>
						</picture>
					</div>

				</div>
				<div class="col-xs-12 col-sm-12 col-md-4">
					<div class="why-choose-us-content wow fadeInUp">
						<h3 class="heading3-border text-uppercase"><?php echo $who_choose_us['why_choose_us_header']; ?></h3>
						<?php echo wpautop( $who_choose_us['why_choose_us_content'] ); ?>
						<!--						<a href="" title="" class="readmore-btn">Далее <i class="fa fa-angle-right"></i></a>-->
					</div>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-4">
					<div class="why-choose-us wow fadeInRight">
						<div class="why-choose-us-icon">
							<i class="fa fa-handshake-o"></i>
							<?php echo $who_choose_us['why_choose_us_reason_1']; ?>
						</div>
						<div class="why-choose-us-icon">
							<i class="fa fa-diamond"></i>
							<?php echo $who_choose_us['why_choose_us_reason_2']; ?>
						</div>
						<div class="why-choose-us-icon">
							<i class="fa fa-thumbs-o-up"></i>
							<?php echo $who_choose_us['why_choose_us_reason_3']; ?>
						</div>
						<div class="why-choose-us-icon">
							<i class="fa fa-map-marker"></i>
							<?php echo $who_choose_us['why_choose_us_reason_4']; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Why Choose end -->

	<!-- Service process start -->
	<div class="wshipping-content-block service-process">
		<?php $order = get_field( 'order' ); ?>
		<div class="container wow fadeInUp">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="section-title">
						<?php echo $order['content']; ?>
					</div>
				</div>
			</div>
			<div class="process-row">
				<div class="process-step">
					<div class="process-icon">
						<span>1</span>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/process-icon1.png"
							 alt="Выбор"/>
					</div>
					<p><?php echo $order['1']; ?></p>
				</div>
				<div class="process-step">
					<div class="process-icon">
						<span>2</span>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/process-icon2.png"
							 alt="Просчет"/>
					</div>
					<p><?php echo $order['2']; ?></p>
				</div>
				<div class="process-step">
					<div class="process-icon">
						<span>3</span>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/process-icon3.png"
							 alt="Оплата"/>
					</div>
					<p><?php echo $order['3']; ?></p>
				</div>
				<div class="process-step">
					<div class="process-icon">
						<span>4</span>
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/process-icon4.png"
							 alt="Получение"/>
					</div>
					<p><?php echo $order['4']; ?></p>
				</div>
			</div>
		</div>
	</div>
	<!-- service process start -->

	<!-- Get Quote start -->
<?php $order_form = get_field( 'order_form' ); ?>
	<div class="wshipping-content-block home-quote" style="background-image: url('<?php echo $order_form['bg'] ?>');">
		<div class="container">
			<div class="row">
				<div class="col-md-6 wow fadeInLeft">
					<?php echo $order_form['header']; ?>
					<div class="quote-form">
						<?php echo do_shortcode( '[contact-form-7 id="53" title="Заказать просчет главная"]' ); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Get quote End -->

	<!-- Latest gallery & post start -->
	<div class="wshipping-content-block">
		<div class="container">
			<div class="row">
				<!-- Latest gallery start -->
				<div class="col-xs-12 col-sm-12 col-md-8 wow fadeInUp">
					<?php $gallery = get_field( 'gallery' ); ?>

					<h3 class="heading3-border text-uppercase"><?php echo $gallery['header']; ?></h3>
					<div class="row equal-full">
						<?php foreach ( $gallery['img'] as $img ) : ?>
							<div class="col-xs-6 col-sm-6 col-md-6 l-gallery-holder">
								<a class="latest-gallery"
								   href="<?php echo $img['url']; ?>"
								   title="<?php if (!empty($img['alt'])) : echo $img['alt']; else : echo $img['title']; endif; ?>"><i class="fa fa-search-plus" aria-hidden="true"></i><img
											src="<?php echo $img['url']; ?>"
											alt="<?php if (!empty($img['alt'])) : echo $img['alt']; else : echo $img['title']; endif; ?>"/>
								</a>
							</div>
						<?php endforeach; ?>

					</div>
				</div>
				<!-- Latest gallery end -->

				<!-- Latest post start -->
				<div class="col-xs-12 col-sm-12 col-md-4 home2-latest-post wow fadeInUp">
					<h3 class="heading3-border text-uppercase">Последнии новости</h3>
					<div class="letest-post">
						<ul>
							<?php
							$args  = array(
								'numberposts' => '4',
								'post_type'   => 'post',
								'post_status' => 'publish',
								'order'       => 'DESC'
							);
							$posts = get_posts( $args );
							foreach ( $posts as $post ) :
								setup_postdata( $post );
								$content = strip_shortcodes( $post->post_content );
								$excerpt = wp_trim_words( $content, $num_words = 20, $more = null );
								?>
								<li>
									<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
										<picture>
											<?php the_post_thumbnail( array( 116, 95 ) ); ?>
										</picture>
										<?php echo wpautop( $excerpt ); ?>
										<span><?php echo get_the_date( 'd F Y' ); ?></span>
									</a>
								</li>

							<?php
							endforeach;
							wp_reset_postdata();
							?>
						</ul>
					</div>
				</div>
				<!-- Latest post end -->
			</div>
		</div>
	</div>
	<!-- Latest post & gallery end -->

<?php get_template_part( 'template-parts/block', 'testimonials' ); ?>

<?php get_template_part( 'template-parts/block', 'clients' ); ?>

<?php get_footer(); ?>